import { createNodeRedisClient } from 'handy-redis'

export const cacheClient = () => createNodeRedisClient(process.env.REDIS_URL || '')