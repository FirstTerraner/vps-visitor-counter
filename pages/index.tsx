import type { GetServerSideProps } from 'next'
import Head from 'next/head'
import Link from 'next/link'
import { cacheClient } from '../src/db'
import styles from '../styles/Home.module.css'
import Image from 'next/image'

interface IVisitor {
	ip: string
	a: string
	d: Date
}

export const getServerSideProps: GetServerSideProps = async ctx => {

	const client = cacheClient()

	const date = new Date()

	const dateStr = date.toLocaleDateString()

	const visitorsEntry = await client.get(dateStr)

	const allVisits = await client.get('AT')

	const visitors: IVisitor[] = JSON.parse(visitorsEntry || '[]')

	return { props: { total: allVisits, today: visitors } }
}

const Home = ({ total, today }: { total: string, today: IVisitor[] }) => {
	return (
		<div className={`${styles.container} ${styles.gonimate} ${styles.fadeIn}`}>
			<Head>
				<title>VPS Visitors</title>
				<meta name="description" content="A simple interface for the visitor counter of https://virtualpro.space" />
				<link rel="icon" href="/favicon.ico" />
			</Head>

			<main className={styles.main}>
				<h1 className={styles.title}>
					<Link href="https://virtualpro.space">
						<a target='_blank'>
							<Image
								src='/appLogoLight.png'
								alt=''
								width={100}
								height={120}
							/>
						</a>
					</Link>
					&nbsp;Visitors
				</h1>

				{/* <p className={styles.description}>
					Get started by editing{' '}
					<code className={styles.code}>pages/index.js</code>
				</p> */}

				<div className={styles.grid}>
					<Link href="/" >
						<a className={styles.card}>
							<h2 className={styles.title}>TOTAL</h2>
							<p className={styles.counter}>{total}</p>
						</a>
					</Link>

					<Link href="/visitors" >
						<a className={styles.card}>
							<h2 className={styles.title}>TODAY</h2>
							<p className={styles.counter}>{today.length}</p>
						</a>
					</Link>
				</div>
			</main>

			<footer className={styles.footer}>
				<a
					href="https://virtualpro.space"
					target="_blank"
					rel="noopener noreferrer"
				>
					<span className={styles.logo}>
						<Image src="/appNameLight.png" alt='' width={150} height={25} />
					</span>
				</a>
			</footer>
		</div>
	)
}

export default Home
