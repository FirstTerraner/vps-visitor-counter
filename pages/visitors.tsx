import type { GetServerSideProps } from 'next'
import Head from 'next/head'
import Link from 'next/link'
import { cacheClient } from '../src/db'
import styles from '../styles/Home.module.css'

interface IVisitor {
	ip: string
	a: string
	d: Date
}

export const getServerSideProps: GetServerSideProps = async ctx => {

	const client = cacheClient()

	const date = new Date()

	const dateStr = date.toLocaleDateString()

	const visitorsEntry = await client.get(dateStr)

	const visitors: IVisitor[] = JSON.parse(visitorsEntry || '[]')

	return { props: { visitors } }
}

const Visitors = ({ visitors }: { visitors: IVisitor[] }) => {
	return (
		<div className={`${styles.container} ${styles.gonimate} ${styles.fadeIn}`}>
			<Head>
				<title>VPS Visitors</title>
				<meta name="description" content="A simple interface for the visitor counter of https://virtualpro.space" />
				<link rel="icon" href="/favicon.ico" />
			</Head>

			<main className={styles.main}>
				<Link
					href='/'
				>
					<a className={styles.backBtn}><p>BACK</p></a>
				</Link>

				{visitors && visitors.length > 0 &&
					<h1 className={styles.title}>
						Today{"'"}s Visitors
					</h1>
				}


				{/* <p className={styles.description}>
					Get started by editing{' '}
					<code className={styles.code}>pages/index.js</code>
				</p> */}

				<div className={styles.vgrid}>
					{visitors && visitors.length > 0 ?
						visitors.map(visitor => (
							<div key={`${visitor.ip}-${visitor.a}`} className={styles.card}>
								<h2 className={styles.title}>{visitor.ip}</h2>
								<h2 className={styles.title}>{visitor.a}</h2>
								<h2 className={styles.title}>{visitor.d}</h2>
							</div>
						))
						:
						<h2 className={styles.title}>No Visitors today =(</h2>
					}
				</div>
			</main>

			{/* <footer className={styles.footer}>
				VPS
				<a
					href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
					target="_blank"
					rel="noopener noreferrer"
				>
					Powered by{' '}
					<span className={styles.logo}>
						<Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
					</span>
				</a>
			</footer> */}
		</div>
	)
}

export default Visitors
